import React, { Component } from "react";

import connector, { ImportedProps } from "../containers/app"

interface Props extends ImportedProps {

}

interface State {
	value: string;
}

class App extends Component<Props, State> {

	constructor(props: Props) {
		super(props);

		this.state = {
			value: "",
		};
	}

	public render(): JSX.Element {
		return (
			<div>
				<div>
					<input type="text" value={this.state.value} onChange={this.handleChange}></input>
					<button onClick={() => {
						this.props.add(this.state.value);
						this.setState({
							value: "",
						});
					}}>Click me</button>
				</div>
				<div>
					{this.props.todos.map((todo: string, index: number) => <div key={index.toString()}>{todo}</div>)}
				</div>
			</div>
		);
	}

	private handleChange = (event: any): void => {
		this.setState({
			value: event.target.value,
		});
	};
}

export default connector(App);
