import { createReducer, CaseReducers } from "@reduxjs/toolkit";

import { add } from "../actions";

export interface State {
	list: Array<string>;
};

const initialState: State = {
	list: [],
};

const actionsMap: CaseReducers<State, any> = {
	[add.type]: (state, action) => {
		state.list.push(`TODO: ${action.payload.content}`);
	},
};

export default createReducer(initialState, actionsMap);
