import { connect } from "react-redux";
import { ConnectedProps } from "react-redux";

import { RootState } from "../reducers";
import { add } from "../actions";

const mapStateToProps = (state: RootState) => ({
	todos: state.todo.list,
});

const mapDispatchToProps = {
	add,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector;

export type ImportedProps = ConnectedProps<typeof connector>;
