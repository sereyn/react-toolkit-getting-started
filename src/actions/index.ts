import { createAction } from "@reduxjs/toolkit";

export const add = createAction("ADD", (content: string) => ({
	payload: {
		content: content.length === 0 ? "Unnamed TODO" : content,
	},
}));
