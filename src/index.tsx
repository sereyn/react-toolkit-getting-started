import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";

import App from "./components/App";

import reducer from "./reducers";

const store = configureStore({
	reducer,
});

render(
	<React.StrictMode>
		<Provider store={store}>
			<App />	
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
);
